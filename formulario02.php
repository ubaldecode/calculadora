<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Procesar PHP dentro del mismo formulario</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/style.css" >
    <link rel="stylesheet" href="css/cbs.css" >
</head>
<body>
    <form action="#" method="POST" >
        <legend>=+-*/Calculadora/*-+=</legend>
        <p>Nro1: <input type="text" name="txtNro1"/></p>
        <p>Nro2: <input type="text" name="txtNro2"/></p>
        <p><input type="submit" name="btnSumar" value="+"/>
        <input type="submit" name="btnRestar" value="-"/>
        <input type="submit" name="btnMultiplicar" value="x"/>
        <p><input type="submit" name="btnDividir" value="/"/>   
        <input type="submit" name="btnFactorial" value="n!"/>
        <input type="submit" name="btnPotencia" value="^"/> 
        <p> <input type="submit" name="btnSeno" value="Sen"/> 
        <input type="submit" name="btnCoseno" value="Cos"/> 
        <input type="submit" name="btnTangente" value="Tan"/>
        <p><input type="submit" name="btnPorcentaje" value="%"/>
        <input type="submit" name="btnRaizCuadrada" value="√"/>
        <input type="submit" name="btnRaizNesima" value="n√x"/> 
        <p><input type="submit" name="btnInverso" value="1/x"/>
        
        
        
        </p>
    </form>    
    <?php 
    if(($_POST))
    {
        //
        // Llamar a la clase Calculadora
        include("calculadora.php");        
        $nro1 = $_POST['txtNro1'];
        $nro2 = $_POST['txtNro2'];
        if(isset($_POST['btnSumar']))
        {
            // Instanciar un objeto a traves de la clase
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $suma = $calculo->Sumar();
            echo ".  La suma de los números es: " , $suma;
        }
        else if(isset($_POST['btnRestar']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $resta = $calculo->Restar();
            echo ".  La resta de los números es: ", $resta;
        }
        else if(isset($_POST['btnMultiplicar']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $multiplica = $calculo->Multiplicar();
            echo ".  La multiplicacion de los números es: ", $multiplica;
        }
        else if(isset($_POST['btnDividir']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $divide = $calculo->Dividir();
            if($nro2 != 0)
            {
            echo ".  La divicion de los números es: ", $divide;
            }
        } 
        else if(isset($_POST['btnFactorial']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $fac = $calculo->Fact($nro1);
            echo ".  El factorial del número es: ", $fac;
        }
        else if(isset($_POST['btnPotencia']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $pot = $calculo->Pot($nro1,$nro2);
            echo ".  La potencia del número es: ", $pot;
        }
        else if(isset($_POST['btnSeno']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $sen = $calculo->Seno($nro1);
            echo ".  El Seno del número es: ", $sen;
        }
        else if(isset($_POST['btnCoseno']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $cos = $calculo->Coseno($nro1);
            echo ".  El Coseno del número es: ", $cos;
        }
        else if(isset($_POST['btnTangente']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $tan = $calculo->Tan($nro1);
            echo ".  El tagente del número es: ", $tan;
        }
        else if(isset($_POST['btnPorcentaje']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $por = $calculo->Porcentaje();
            echo ".  El porcentaje del número es: ", $por;
        }
        else if(isset($_POST['btnRaizCuadrada']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $raiz = $calculo->RaizCuadrada();
            echo ".  La raiz cuadrada del número es: ", $raiz;
        }
        else if(isset($_POST['btnRaizNesima']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $raizn = $calculo->RaizNesima();
            echo ".  La Raiz N-esima del número es: ", $raizn;
        }

        else if(isset($_POST['btnInverso']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $inv = $calculo->Inverso();
            echo ".  El Inverso del número es: ", $inv;
        }
    }
    ?>
</body>
</html>
