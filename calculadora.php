<?php
    class Calculadora
    {
        //atributos
        public $nro1;
        public $nro2;
        // Metodos u operaciones
        public function Sumar()
        {
            return $this->nro1 + $this->nro2;
        }

        public function Restar()
        {
            return $this->nro1 - $this->nro2;
        }

         //Multiplicar y dividir
         public function Multiplicar()
         {
             return $this->nro1 * $this->nro2;
         }
         public function Dividir()
         {
            if($this->nro2 == 0)
            {
                echo ".  ERROR en la Divicion entre 0";
            }
            else
            {
                return $this->nro1 / $this->nro2;
            }
                 
         }
         public function Fact($nro)
         {
             if($nro == 0)
                 return 1;
             else
                 return $nro * $this->Fact($nro - 1);
         }
         //potencia, seno y tangente
         public function Pot($nro1,$nro2)
         {
             if($nro2 == 0)
                 return 1;
             else
                 return $nro1 * $this->Pot($nro1,$nro2-1);
         }
         public function Seno($nro1)
         {
             return(sin(deg2rad($nro1)));
         }
         public function Coseno($nro1)
         {
           return (cos(($nro1 * pi()) / 180));
         }
         public function Tan($nro1)
         {
             return(tan(($nro1*pi()/180)));
         }
         public function Porcentaje()
         {
             return ($this->nro1*$this->nro2/100);
         }
         public function RaizCuadrada()
         {
            return (sqrt($this->nro1));
         }
         public function RaizNesima()
         {
            return (pow($this->nro1, 1/$this->nro2));
         }
         public function Inverso()
         {
            return (1/$this->nro1);
         }     
     }
?>